<?php

namespace App\Http\Resources;

class Planet{

    public $name;    
    public $rotation_period;    
    public $orbital_period;    
    public $diameter;    
    public $climate;    
    public $gravity;    
    public $terrain;    
    public $surface_water;    
    public $population;    

    public function __construct($Planet){
        
        $this->name = $Planet['name'];
        $this->rotation_period = (int) $Planet['rotation_period'];
        $this->orbital_period = (int) $Planet['orbital_period'];
        $this->diameter = (int) $Planet['diameter'];
        $this->climate = $Planet['climate'];
        $this->gravity = $Planet['gravity'];
        $this->terrain = $Planet['terrain'];
        $this->surface_water = (int) $Planet['surface_water'];
        $this->population = (int) $Planet['population'];

    }

}
