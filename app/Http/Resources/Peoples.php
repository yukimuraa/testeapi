<?php

namespace App\Http\Resources;

class Peoples{

    public $peoples;

    public function __construct($Peoples){

        foreach ($Peoples['results'] as $key => $people) {
            $this->peoples[] = new People( $people );
        }

    }

    public function get(){
        return $this->peoples;
    }

}