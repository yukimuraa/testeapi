<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\Peoples;
use App\Components\SwapApi;

class HomeController extends Controller{

    public function get(Request $request){

        $SwapApi = new SwapApi();

        $Peoples = new Peoples( $SwapApi->getPeoples() );

        return view('home')->with( 'peoples', $Peoples->get() );
    }
}
