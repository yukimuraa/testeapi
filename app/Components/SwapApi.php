<?php

namespace App\Components;

class SwapApi{

    private $_url_base = 'https://swapi.co/api/';

    public function getPeoples(){

        $url = $this->_url_base . 'people';

        $data = $this->get($url);

        return $data;

    }

    public function getPeople(int $id){

        $url = $this->_url_base . 'people/' . $id;

        $data = $this->get($url);

        return $data;

    }

    public function getPlanet($url){
        
        $data = $this->get($url);

        return $data;
    }

    private function get($url){

        $response = file_get_contents($url);

        return json_decode($response, true);
    }
}