<?php

namespace App\Http\Resources;

use App\Components\SwapApi;

class People{

    public $name;
    public $height;
    public $mass;
    public $hair_color;
    public $skin_color;
    public $eye_color;
    public $birth_year;
    public $gender;
    public $homeworld;
    public $films;
    public $species;
    public $vehicles;
    public $starships;

    public $url;

    public function __construct($People){ 
        $SwapApi = new SwapApi();
        
        $this->name = $People['name'];
        $this->height = (int) $People[ 'height'];
        $this->mass = (int) $People['mass'];
        $this->hair_color = $People['hair_color'];
        $this->skin_color = $People['skin_color'];
        $this->eye_color = $People['eye_color'];
        $this->birth_year = $People['birth_year'];
        $this->gender = $People['gender'];
        $this->name = $People['name'];
        $this->homeworld = $this->getHomeworld($People['homeworld']);
        $this->url = $People['url'];

    }

    private function getHomeworld($url){
        $SwapApi = new SwapApi();

        $planet = $SwapApi->getPlanet($url);

        return new Planet($planet);
    }
}
